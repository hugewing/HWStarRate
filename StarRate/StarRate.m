//
//  StarRate.m
//  iStudy
//
//  Created by LiTengFei on 15/1/20.
//  Copyright (c) 2015年 HugeWing Co.Ltd. All rights reserved.
//

#import "StarRate.h"


@implementation StarRate

@synthesize default_space_width;
@synthesize default_space_height;

@synthesize default_between_space;

@synthesize star_width;
@synthesize star_height;

- (void)commonPare:(CGRect)frame {
    default_space_width = 4;
    star_height = 15;
    star_width = 15;
    default_between_space = (CGRectGetHeight(frame) - star_height ) / 2 ;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self  = [super init];
    if(self){
        self.translatesAutoresizingMaskIntoConstraints = NO;
        self.starImage = [UIImage imageNamed:@"1"];
        self.startHeightImage = [UIImage imageNamed:@"2"];

        default_space_width = 4;
        star_height = 15;
        star_width = 15;
        default_between_space = 10;

        [self commonInstance];

        self.maxRating = 5;
    }
    return self;
}
-(instancetype)init{
    self  = [super init];
    if(self){
        self.starImage = [UIImage imageNamed:@"1"];
        self.startHeightImage = [UIImage imageNamed:@"2"];

        default_space_width = 4;
        star_height = 15;
        star_width = 15;
        default_space_height = 2;
        [self commonInstance];

        self.maxRating = 5;
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.starImage = [UIImage imageNamed:@"2"];
        self.startHeightImage = [UIImage imageNamed:@"1"];

        [self commonPare:frame];
        [self commonInstance];

        self.maxRating = 5;
    }
    return self;
}


-(void)commonInstance{
    for (int i = 1; i<= self.maxRating; i++) {
        UIButton * star = [[UIButton alloc]init];
        star.userInteractionEnabled = NO;
        [star setImage:self.starImage forState:UIControlStateNormal];
        [star setImage:self.startHeightImage forState:UIControlStateSelected];
        [star addTarget:self action:@selector(starClick:) forControlEvents:UIControlEventTouchUpInside];

        star.tag = i;
        [self addSubview:star];
    }
}

-(void)setRate:(NSInteger)rate{
    _rate = rate;
    for (int i=1; i<=self.maxRating; i++) {
        UIButton * imageView = (UIButton *)[self viewWithTag:i];
        if(i <= rate){
            imageView.selected = YES;
        }else{
            imageView.selected = NO;
        }
    }
}
-(void)setMaxRating:(NSInteger)maxRating{
    _maxRating = maxRating;
    for (int i=1; i<=self.maxRating; i++) {
        UIButton * imageView = (UIButton *)[self viewWithTag:i];
        if(imageView==nil){
            UIButton * star = [[UIButton alloc]init];
            star.userInteractionEnabled = NO;
            [star setImage:self.starImage forState:UIControlStateNormal];
            [star setImage:self.startHeightImage forState:UIControlStateSelected];
            [star addTarget:self action:@selector(starClick:) forControlEvents:UIControlEventTouchDown|UIControlEventTouchDragEnter];
            star.tag = i;
            star.selected = NO;
            [self addSubview:star];
        }else {
            imageView.selected = NO;
        }
    }
}
-(void)starClick:(UIButton *)button{
    for (int i=1; i<=self.maxRating; i++) {
        UIButton * imageView = (UIButton *)[self viewWithTag:(i)];
        self.rate = button.tag;
        if(i <= button.tag){
            imageView.selected = YES;
        }else{
            imageView.selected = NO;
        }
    }
}
-(void)layoutSubviews{
    [super layoutSubviews];
    for (int i=1; i<=self.maxRating; i++) {
        UIButton * imageView = (UIButton *)[self viewWithTag:(i)];
        imageView.frame = CGRectMake(default_space_width +  (star_width +default_between_space)* (i-1)  , default_space_height, star_width, star_height);
        if(i <= _rate){
            imageView.selected = YES;
        }else{
            imageView.selected = NO;
        }
    }
}

-(void)setWritable:(BOOL)writable{
    _writable = writable;
    for (int i=1; i<=self.maxRating; i++) {
        UIButton * imageView = (UIButton *)[self viewWithTag:(i)];
        imageView.userInteractionEnabled = YES;
    }
}
@end
