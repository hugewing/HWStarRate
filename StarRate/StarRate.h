//
//  StarRate.h
//  iStudy
//
//  Created by LiTengFei on 15/1/20.
//  Copyright (c) 2015年 HugeWing Co.Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+StarColor.h"

@interface StarRate : UIView

@property(nonatomic, assign) NSInteger maxRating;
@property(nonatomic, assign) NSInteger rate;
@property(nonatomic, strong) UIImage *starImage;
@property(nonatomic, strong) UIImage *startHeightImage;

@property(nonatomic, assign) CGFloat default_space_width;
@property(nonatomic, assign) CGFloat default_space_height;

@property(nonatomic, assign) CGFloat star_width;
@property(nonatomic, assign) CGFloat star_height;

@property(nonatomic, assign) CGFloat default_between_space;

@property(nonatomic, assign) BOOL writable;
@end
