//
//  UIColor+StarColor.m
//  iStudy
//
//  Created by LiTengFei on 15/1/21.
//  Copyright (c) 2015年 HugeWing Co.Ltd. All rights reserved.
//

#import "UIColor+StarColor.h"

@implementation UIColor (StarColor)
+(UIColor *)defaultRedColor{
    return [UIColor colorWithRed:(211) / 255.0 green:(78) / 255.0 blue:(89) / 255.0 alpha:(1.0)];
}
@end
