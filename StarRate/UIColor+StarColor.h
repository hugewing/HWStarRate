//
//  UIColor+StarColor.h
//  iStudy
//
//  Created by LiTengFei on 15/1/21.
//  Copyright (c) 2015年 HugeWing Co.Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (StarColor)
+ (UIColor *)defaultRedColor;
@end
