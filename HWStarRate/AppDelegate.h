//
//  AppDelegate.h
//  HWStarRate
//
//  Created by LiTengFei on 15/2/21.
//  Copyright (c) 2015年 LiTengFei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

