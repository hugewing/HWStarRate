//
//  ViewController.m
//  HWStarRate
//
//  Created by LiTengFei on 15/2/21.
//  Copyright (c) 2015年 LiTengFei. All rights reserved.
//

#import "ViewController.h"
#import "StarRate.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    StarRate * starRatr = [[StarRate alloc]initWithFrame:CGRectMake(30, 30, 200, 50)];
    starRatr.default_between_space  = 10;

    [self.view addSubview:starRatr];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
