//
//  main.m
//  HWStarRate
//
//  Created by LiTengFei on 15/2/21.
//  Copyright (c) 2015年 LiTengFei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
